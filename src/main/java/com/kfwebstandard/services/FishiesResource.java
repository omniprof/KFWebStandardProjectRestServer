package com.kfwebstandard.services;

import com.kfwebstandard.entities.Fish;
import com.kfwebstandard.jpacontroller.FishActionBeanJPA;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author omni_
 */
@Path("fishies")
@RequestScoped
public class FishiesResource {

    @Inject
    private FishActionBeanJPA fab;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of FishiesResource
     */
    public FishiesResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.kfwebstandard.services.FishiesResource
     *
     * @return an instance of java.util.List
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Fish> getJson() {
        return fab.getAll();
    }
}
